package com.mayaswell.loopadora.widget;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.loopadora.MainActivity;
import com.mayaswell.loopadora.R;
import com.mayaswell.loopadora.SampleClip;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.widget.KavePot;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by dak on 10/8/2016.
 */
public class SampleItemView extends RelativeLayout{
	private static final int[] STATE_ACTIVE = {R.attr.state_active};
	private static final int[] STATE_PLAYING = {R.attr.state_playing};

	protected Bufferator.SampleInfo sampleInfo;
	protected LPSampleView sampleView;
	protected KavePot gainPot;
	protected KavePot panPot;
	private TextView sampleTitleView = null;
	private ArrayList<SampleClip> sampleClips = null;
//	private SamplePlayer player = null;
//	private float touchX;
//	private float touchY;
	private GestureDetectorCompat detector;
//	private GyesztorDetector detector;
	private SampleItemView self;
	private boolean inScroll = false;

	public SampleItemView(Context context) {
		super(context);
		init(context, null, -1);
	}

	public SampleItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, -1);
	}

	public SampleItemView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
		LayoutInflater.from(context).inflate(R.layout.sample_list_item, this, true);
		setBackgroundResource(R.drawable.sample_list_item_bg);
		sampleView = (LPSampleView) findViewById(R.id.sampleView);
		gainPot = (KavePot) findViewById(R.id.gainControl);
		panPot = (KavePot) findViewById(R.id.panControl);
		sampleTitleView = (TextView) findViewById(R.id.titleView);
		LayoutParams rl = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		rl.setMargins(0, 5, 0, 5);
		setLayoutParams(rl);
		self = this;
		detector = new GestureDetectorCompat(getContext(), new GestureDetector.OnGestureListener() {
			@Override
			public boolean onDown(MotionEvent e) {
				return false;
			}

			@Override
			public void onShowPress(MotionEvent e) {
			}

			@Override
			public boolean onSingleTapUp(MotionEvent e) {
				Log.d("SampleItemView", self.toString()+" onSingleTapUp() ");
				return false;
			}

			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
				return false;
			}

			@Override
			public void onLongPress(MotionEvent ev) {
				if (!inScroll) {
					try {
						((MainActivity) getContext()).select(self);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
				Log.d("SampleItemView", self.toString()+" fling() "+velocityX+", "+velocityY);
				return false;
			}
		});
		/*
		detector = new GyesztorDetector(new GyesztorDetector.Listener() {
			@Override
			public boolean onDown(MotionEvent e) {
				return true;
			}

			@Override
			public void onShowPress(MotionEvent e) {
				Log.d("SampleItemView", self.toString()+" onShowPress() "+e.toString());
			}

			@Override
			public boolean onSingleTapUp(MotionEvent ev) {
				Log.d("SampleItemView", self.toString()+" onSingleTapUp() "+inScroll);
				if (!inScroll) {
					try {
						((MainActivity) getContext()).select(self);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
					return true;
				}
				return false;
			}

			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
				return false;
			}

			@Override
			public void onLongPress(MotionEvent ev) {
				if (!inScroll) {
					Log.d("siv", "on long");
					try {
						((Activity)getContext()).openContextMenu(self);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
				Log.d("SampleItemView", self.toString()+" fling() "+velocityX+", "+velocityY);
				return false;
			}

			@Override
			public boolean onSingleTapConfirmed(MotionEvent e) {
				Log.d("SampleItemView", self.toString()+" onSingleTapConfirmed() "+e.toString());
				return false;
			}

			@Override
			public boolean onDoubleTap(MotionEvent e) {
				Log.d("SampleItemView", self.toString()+" onDoubleTap() "+e.toString());
				return false;
			}

			@Override
			public boolean onDoubleTapEvent(MotionEvent e) {
				Log.d("SampleItemView", self.toString()+" onDoubleTapEvent() "+e.toString());
				return false;
			}
		});
		*/
		refreshDrawableState();
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		inScroll = false;
//		Log.d("SampleItemView", self.toString()+" onInterceptTouchEvent()");
		return super.onInterceptTouchEvent(ev);
	}

	@Override
	protected void onAttachedToWindow() {
		ViewParent parent = getParent();
		if (parent != null) {
			((RecyclerView)parent).addOnScrollListener(new RecyclerView.OnScrollListener() {
				@Override
				public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
					inScroll = true;
					super.onScrollStateChanged(recyclerView, newState);
				}
			});
		}
		super.onAttachedToWindow();
	}

	public void setTo(Bufferator.SampleInfo q, ArrayList<SampleClip> r, boolean selected) {
		setSelected(selected);
		sampleInfo = q;
		sampleClips = r;
//		Log.d("SampleItemView", "setting sampleInfo state "+p.path);
		File f = new File(q.path);
		sampleTitleView.setText(f.getName());
		if (sampleView != null) {
			sampleView.setTo(q);
//			sampleView.setPlayer(r);
		}
	}

	public boolean selectClip(int i) {
		if (sampleClips == null || i < 0 || i >= sampleClips.size()) return false;
		final SampleClip c = sampleClips.get(i);
		if (gainPot != null) gainPot.setValue(c.gain);
		if (panPot != null) panPot.setValue(c.pan);
		return true;
	}

	/**
	 * @see android.widget.TextView#onCreateDrawableState(int)
	 */
	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
///		Log.d("SampleItemView", "onCreateDrawableState "+toString()+" active:"+isActive()+", play:"+isPlaying()+", select:"+isSelected());
/*
		if (isActive()) {
			mergeDrawableStates(drawableState, STATE_ACTIVE);
		}
		if (isPlaying()) {
			mergeDrawableStates(drawableState, STATE_PLAYING);
		}
		*/
		return drawableState;
	}

	public String toString() {
		String nm = "<>";
		if (sampleInfo != null && sampleInfo.path != null) {
			nm = sampleInfo.path.toString();
			int n = nm.lastIndexOf('/');
			if (n > 0) {
				nm = nm.substring(n);
			}
		}
		return "sv "+ nm;
	}

	/**
	 */
	@Override
	protected void drawableStateChanged ()
	{
		super.drawableStateChanged();
	}


	public boolean select() {
		setSelected(true);
		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event){
		return detector.onTouchEvent(event);
/*
		int action = MotionEventCompat.getActionMasked(event);

		switch(action) {
			case (MotionEvent.ACTION_DOWN) :
				touchX = event.getX();
				touchY = event.getY();
				Log.d("SampleItemView",toString()+" ACTION_DOWN "+touchX+", "+touchY);
				return super.onTouchEvent(event);
			case (MotionEvent.ACTION_MOVE) :
				float x = event.getX();
				float y = event.getY();
				Log.d("SampleItemView",toString()+" ACTION_MOVE "+x+", "+y);
				return super.onTouchEvent(event);
			case (MotionEvent.ACTION_UP) :
				Log.d("SampleItemView",toString()+" ACTION_UP");
				return true;
			case (MotionEvent.ACTION_CANCEL) :
				Log.d("SampleItemView",toString()+" ACTION_CANCEL");
				return true;
			case (MotionEvent.ACTION_OUTSIDE) :
				Log.d("SampleItemView",toString()+" ACTION_OUTSIDE");
				return true;
			default :
				Log.d("SampleItemView",toString()+" something else");
				return super.onTouchEvent(event);
		}
		*/
	}

	public Bufferator.SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	public void updateCursor() {
//		sampleView.updateCursor();
	}
}