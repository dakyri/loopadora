package com.mayaswell.loopadora.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.mayaswell.spacegun.PadSample;

import java.util.ArrayList;

/**
 * Created by dak on 3/10/2017.
 */
public class PadControlView extends RelativeLayout {
	private int highestChildId = 1;
	private PadStripView lastView = null;

	public PadControlView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public PadControlView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public PadControlView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle) {
		setBackgroundColor(0x00ff00);
	}

	public void setPadList(ArrayList<PadSample> padSampleList) {
		Log.d("PadControlView", "setPadList, size "+(padSampleList != null? padSampleList.size():0));
		highestChildId = 1;
		lastView = null;
		for (PadSample p: padSampleList) {
			addPadStripView(p);
		}
	}

	private PadStripView addPadStripView(PadSample p) {
		Log.d("PadControlView", "adding "+p.toString());
		PadStripView psv = new PadStripView(getContext());
		psv.setPadSample(p);
		psv.setId(highestChildId++);
		LayoutParams rlp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		if (lastView == null) {
			rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		} else {
			rlp.addRule(RelativeLayout.RIGHT_OF, lastView.getId());
		}
		addView(psv, rlp);
		lastView = psv;
		return psv;
	}

}
