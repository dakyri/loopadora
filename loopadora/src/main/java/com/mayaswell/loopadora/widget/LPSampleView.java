package com.mayaswell.loopadora.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.mayaswell.widget.BaseSampleView;

/**
 * Created by dak on 4/11/2017.
 */
public class LPSampleView extends BaseSampleView {
	public LPSampleView(Context context) {
		super(context);
	}

	public LPSampleView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public LPSampleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
	}

}
