package com.mayaswell.loopadora.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.mayaswell.loopadora.R;
import com.mayaswell.spacegun.PadSample;
import com.mayaswell.widget.KavePot;

/**
 * Created by dak on 3/11/2017.
 */
public class PadStripView extends RelativeLayout {
	KavePot gainControl; // todo maybe extract an interface common to both this and TeaPot
	KavePot panControl; // todo maybe extract an interface common to both this and TeaPot
	PadStateButton padStateButton;
	private PadSample padSample = null;

	public PadStripView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public PadStripView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public PadStripView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle) {
		LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.pad_strip, this, true);

		gainControl = (KavePot)findViewById (R.id.gainControl);
		panControl = (KavePot)findViewById (R.id.panControl);
		padStateButton = (PadStateButton) findViewById(R.id.padStateButton);

	}

	public void setPadSample(PadSample padSample) {
		this.padSample = padSample;
		this.padStateButton.setPadSample(padSample);
	}
}
