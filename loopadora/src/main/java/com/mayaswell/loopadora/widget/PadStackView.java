package com.mayaswell.loopadora.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by dak on 3/15/2017.
 */
public class PadStackView extends View {
	public PadStackView(Context context) {
		super(context);
	}

	public PadStackView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PadStackView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}
}
