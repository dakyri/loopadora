package com.mayaswell.loopadora.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageButton;

import com.mayaswell.loopadora.R;
import com.mayaswell.spacegun.PadSample;
import com.mayaswell.spacegun.PadSampleState;

/**
 * Created by dak on 3/15/2017.
 */
public class PadStateButton extends ImageButton {
	private static final String schemaName = "http://schemas.android.com/apk/res-auto";
	protected Rect vRect = new Rect();
	protected Rect pdRect = new Rect();
	protected float textSmallScaledPx = 10;
	protected float textLargeScaledPx = 15;
	protected PadSample padSample = null;
	private Paint textBrush = null;
	private Paint positionBrush = null;
	protected int textColor = 0xdd4285F4;
	protected int positionColor = 0xff333377;

	public PadStateButton(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public PadStateButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public PadStateButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}

	/**
	 *
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	protected void setup(Context context, AttributeSet attrs, int defStyle) {
		setTextSize(10);

		if (attrs != null) {
			int id = attrs.getAttributeResourceValue(schemaName, "textColor", -1);
			if (id >= 0) {
				textColor  = ContextCompat.getColor(getContext(), id);
			}
			id = attrs.getAttributeResourceValue(schemaName, "positionColor", R.color.padLightYellow);
			positionColor  = ContextCompat.getColor(getContext(), id);
		} else {
			positionColor  = ContextCompat.getColor(getContext(), R.color.padLightYellow);
		}

		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setColor(textColor);
		textBrush.setTextSize(textSmallScaledPx);
		textBrush.setTextAlign(Paint.Align.CENTER);

		positionBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		positionBrush.setColor(positionColor);
		positionBrush.setStyle(Paint.Style.STROKE);
		positionBrush.setStrokeWidth(4);
	}

	/**
	 *
	 * @param textSize
	 */
	public void setTextSize(float textSize) {
		textSmallScaledPx = textSize * getContext().getResources().getDisplayMetrics().density;
	}

	/**
	 *
	 * @param padSample
	 */
	public void setPadSample(PadSample padSample) {
		this.padSample = padSample;
	}

	/**
	 * @param canvas
	 */
	@Override
	protected void onDraw (Canvas canvas)
	{
		super.onDraw(canvas);
		getLocalVisibleRect(vRect);
		vRect.offset(0, 0);
		pdRect.set(
				vRect.left + 3, 	(int) (vRect.top + textSmallScaledPx + 3),
				vRect.right - 3,	(int) (vRect.top + textSmallScaledPx + 0.5* textSmallScaledPx + 3));
//		Log.d("pad button", String.format("pdr %d %d %d %d", pdRect.left, pdRect.top, pdRect.right, pdRect.bottom));
//		Log.d("pad button", String.format("vr %d %d %d %d", vRect.left, vRect.top, vRect.right, vRect.bottom));
		if (padSample != null) {
//			Log.d("draw button", String.format("playing %b", padSample.isPlaying()));
			if (padSample.isPlaying()) {
				float lp = pdRect.left;
				float rp = pdRect.left+padSample.getCurrentPosition()*(pdRect.right-pdRect.left);
				float mp = (pdRect.bottom+pdRect.top)/2;
//				canvas.drawLine(lp, pdRect.top, lp, pdRect.bottom, positionBrush);
//				canvas.drawLine(rp, pdRect.top, rp, pdRect.bottom, positionBrush);
				canvas.drawLine(lp, mp, rp, mp, positionBrush);
			}
			PadSampleState pss = padSample.getState();
			if (pss != null) {
				if (pss.syncMasterId >= 0) {
					textBrush.setTextSize(textSmallScaledPx);
					canvas.drawText(Integer.toString(pss.syncMasterId), vRect.right - 23, vRect.top + textSmallScaledPx, textBrush);
				}
//				if (pss.name != null) {
//					canvas.drawText(pss.name, (vRect.right + vRect.left) / 2, vRect.bottom - textSmallScaledPx, textBrush);
//				}
			}
		}
	}


}
