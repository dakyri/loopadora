package com.mayaswell.loopadora.widget;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.Bufferator.SampleGfxInfo;
import com.mayaswell.audio.Bufferator.SampleInfo;
import com.mayaswell.loopadora.MainActivity;
import com.mayaswell.loopadora.SampleClip;
import com.mayaswell.spacegun.PadSample;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.widget.TeaPot;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by dak on 6/1/2016.
 */
public class SampleItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

	public static class SampleViewHolder extends RecyclerView.ViewHolder {
		public SampleViewHolder(RelativeLayout v) {
			super(v);
			setIsRecyclable(false);
			v.setOnCreateContextMenuListener(((Activity)v.getContext()));
		}
	}

//	protected ArrayList<PadSample> padList = null;
//	private ArrayList<PadSampleState> padStateList = null;
	private ArrayList<SampleInfo> sampleInfoList = null;

	private HashMap<SampleInfo, ArrayList<SampleClip>> sampleInfoMap = null;

	public void setSampleInfoList(ArrayList<SampleInfo> sl,  HashMap<SampleInfo, ArrayList<SampleClip>> sm) {
		sampleInfoList = sl;
		sampleInfoMap = sm;
		if (sl != null) {
			for (int i = 0; i < sl.size(); i++) {
				notifyItemChanged(i);
				/*
				PadSample p = pl.get(i);
				if (!equalState(p.getState(), sampleInfo(i))) {
//					setPadState(i, p.getState(), p.getSampleInfo());
					notifyItemChanged(i);
				}*/
			}
		}
	}

	/**
	 * @param sampleInfo
	 */
	public void setGfx4SampleInfo(SampleInfo sampleInfo) {
//		Log.d("SampleAdapter", "setting gragix");
		if (sampleInfoList != null) {
			for (int i = 0; i < sampleInfoList.size(); i++) {
				SampleInfo p = sampleInfoList.get(i);
				if (p.path != null && p.path.equals(sampleInfo.path)) {
				// or p == sampleInfo ??? todo xxx
//					Log.d("adapter", "gx item changed");
					notifyItemChanged(i);
					return;
				}
			}
		}
	}

	public void onSampleInfoAdded(SampleInfo si) {
		notifyDataSetChanged();
//		Log.d("SampleAdapter", "onSampleInfoAdded "+ps.path+", "+padStateList.size());
		if (sampleInfoList == null) return;
		int i;
		if ((i = sampleInfoList.indexOf(si)) >= 0) {
//			setPadState(i, pad.state, pad.getSampleInfo());
			notifyItemChanged(i);
		}
	}

	public void onUpdateSampleInfo(SampleInfo ps) {
		if (sampleInfoList == null) return;
		int i = sampleInfoList.indexOf(ps);
		if (i >= 0) {
//			setPadState(i, pad.state, pad.getSampleInfo());
			notifyItemChanged(i);
		}
	}

	public void onSampleInfoPreRemove(SampleInfo ps) {
		if (sampleInfoList == null) return;
		int i = sampleInfoList.indexOf(ps);
		Log.d("adapter", "remove pad state "+i+" "+ps.path);
		if (i >= 0) {
			notifyItemRemoved(i); // afics this should work but doesn't
			notifyDataSetChanged(); // but this does
		}
	}

	public void onSampleClipAdded(SampleClip sc) {
		if (sampleInfoList == null) return;
		int i;
		if ((i = sampleInfoList.indexOf(sc)) >= 0) {
//			setPadState(i, pad.state, pad.getSampleInfo());
			notifyItemChanged(i);
		}
	}

	public void updateSampleClip(SampleClip sc) {
		if (sampleInfoList == null) return;
		int i = sampleInfoList.indexOf(sc);
		if (i >= 0) {
//			setPadState(i, pad.state, pad.getSampleInfo());
			notifyItemChanged(i);
		}
	}

	public void onRemoveSampleClip(SampleClip sc) {
		if (sampleInfoList == null) return;
		int i = sampleInfoList.indexOf(sc);
		if (i >= 0) {
//			setPadState(i, pad.state, pad.getSampleInfo());
			notifyItemChanged(i); // afics this should work but doesn't
		}
	}

	private SampleInfo sampleInfo(int i) {
		return (i>=0 && sampleInfoList != null && i<sampleInfoList.size()? sampleInfoList.get(i): null);
	}

	private ArrayList<SampleClip> sampleClips(int i) {
		SampleInfo s = sampleInfo(i);
		return (s != null && sampleInfoList != null? sampleInfoMap.get(s): null);
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//		Log.d("SampleAdapter", "onCreateViewHolder "+viewType+", "+padStateList.size());
		RecyclerView.ViewHolder vh = null;
		RelativeLayout v = new SampleItemView(parent.getContext());
		vh = new SampleViewHolder(v);
		return vh;
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//		Log.d("SampleAdapter", "onBindViewHolder "+position+", "+padStateList.size());
		if (position >= sampleInfoList.size()) {
			return;
		}
//		int type = getItemViewType(position);
		SampleInfo s = sampleInfo(position);
		SampleItemView itemView = (SampleItemView)((SampleViewHolder) holder).itemView;
		boolean sel = false; //(s != null) && ((MainActivity)itemView.getContext()).getSelectedState() == s;
//		Log.d("SampleAdapter", "onBindViewHolder selected is "+sel);
		itemView.setTo(sampleInfo(position), sampleClips(position), sel);
	}

	@Override
	public void onViewRecycled(RecyclerView.ViewHolder holder) {
		holder.itemView.setOnLongClickListener(null);
		super.onViewRecycled(holder);
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	@Override
	public int getItemCount() {
		if (sampleInfoList == null || sampleInfoList.size() == 0) {
			return 0;
		}
		int i=sampleInfoList.size()-1;
		for (;i>=0; i--) {
			SampleInfo p = sampleInfoList.get(i);
			if (p != null && p.path != null && !p.path.equals("")) {
				break;
			}
		}
		return i+1;
	}

}
