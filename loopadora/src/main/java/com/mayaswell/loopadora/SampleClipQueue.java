package com.mayaswell.loopadora;

import java.util.ArrayList;

/**
 * Created by dak on 4/12/2017.
 */
public class SampleClipQueue {
	private final ArrayList<SampleClip> clip = new ArrayList<SampleClip>();
	private int nextClip = -1;

	public synchronized boolean addClip(SampleClip c) {
		boolean b = clip.add(c);
		if (nextClip < 0) {
			nextClip = 0;
		}
		return b;
	}

	public synchronized boolean addClip(SampleClip c, int ind) {
		boolean b = true;
		if (ind < 0 || ind >= clip.size()) {
			b = clip.add(c);
		} else {
			b = true;
			clip.add(ind, c);
		}
		if (nextClip < 0) {
			nextClip = 0;
		}
		return b;
	}

	public synchronized boolean removeClip(SampleClip c) {
		boolean b =  clip.remove(c);
		if (nextClip >= clip.size()) {
			nextClip = clip.size()-1;
		}
		return b;
	}

	public synchronized SampleClip removeClip(int i) {
		SampleClip sc =  clip.remove(i);
		if (nextClip >= clip.size()) {
			nextClip = clip.size()-1;
		}
		return sc;
	}

	public synchronized void clear() {
		clip.clear();
		nextClip = -1;
	}

	public synchronized SampleClip next() {
		if (clip.size() == 0) {
			return null;
		}
		if (nextClip < 0) nextClip = 0;
		if (nextClip >= clip.size()) {
			nextClip = 0;
		}
		SampleClip c = clip.get(nextClip);
		nextClip++;
		return c;
	}

	public synchronized void reset() {
		nextClip = 0;
	}

	public synchronized void addClipToHead(SampleClip sc) {
		addClip(sc, nextClip);
	}
}
