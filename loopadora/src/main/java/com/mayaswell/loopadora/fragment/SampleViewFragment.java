package com.mayaswell.loopadora.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mayaswell.audio.Bufferator;
import com.mayaswell.loopadora.R;
import com.mayaswell.loopadora.widget.SampleItemAdapter;
import com.mayaswell.spacegun.PadSample;

import java.util.ArrayList;

/**
 * Created by dak on 5/30/2016.
 */
public class SampleViewFragment extends Fragment {
	private SampleItemAdapter sampleItemAdapter	= new SampleItemAdapter();
	private LinearLayoutManager sampleItemLayoutManager	= null;
	private RecyclerView sampleItemView	= null;

	public void setGfx4SampleInfo(Bufferator.SampleInfo sampleInfo) {
		sampleItemAdapter.setGfx4SampleInfo(sampleInfo);
	}

	public void onPadSampleLoaded(PadSample padSample) {
		sampleItemAdapter.onPadSampleLoaded(padSample);
	}

	public void setPadList(ArrayList<PadSample> padList) {
		sampleItemAdapter.setPadList(padList);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.sample_view_fragment, container, true);
		sampleItemView = (RecyclerView) v.findViewById(R.id.sampleItemListView);
		sampleItemView.setHasFixedSize(true);
		sampleItemLayoutManager = new LinearLayoutManager(getActivity());
		sampleItemView.setLayoutManager(sampleItemLayoutManager);
		sampleItemView.setAdapter(sampleItemAdapter);

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
	}

}
