package com.mayaswell.loopadora;

import android.os.Parcel;
import android.os.Parcelable;

import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.Bufferator.SampleInfo;

/**
 * Created by dak on 4/12/2017.
 */
public class SampleClip implements Parcelable{
	public final SampleInfo sample;
	public float gain;
	public float pan;
	public long clipStart;
	public long clipLength;
	public int color;

	public SampleClip(Bufferator.SampleInfo inf) {
		this.sample = inf;
		clipStart = 0;
		clipLength = inf.nTotalFrames;
		color = 0;
		gain = 1;
		pan = 0;
	}

	public SampleClip(Parcel in) {
		String p = in.readString();
		gain = in.readFloat();
		pan = in.readFloat();
		clipStart = in.readLong();
		clipLength = in.readLong();

		this.sample = Bufferator.load(p);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int i) {
		dest.writeString(sample.path);
		dest.writeFloat(gain);
		dest.writeFloat(pan);
		dest.writeLong(clipStart);
		dest.writeLong(clipLength);
		dest.writeInt(color);
	}

	public static Parcelable.Creator CREATOR = new Parcelable.Creator() {

		@Override
		public SampleClip createFromParcel(Parcel in) {
			return new SampleClip(in);
		}

		@Override
		public SampleClip[] newArray(int i) {
			return new SampleClip[i];
		}
	};

}
