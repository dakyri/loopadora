package com.mayaswell.loopadora;

import android.util.Log;
import android.util.Xml;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable.RXControlAssign;
import com.mayaswell.audio.Controllable.SXControlAssign;
import com.mayaswell.audio.Controllable.XYControlAssign;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.FX;
import com.mayaswell.spacegun.BusState;
import com.mayaswell.spacegun.CControl;
import com.mayaswell.spacegun.PadBank;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.util.MWUtils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author dak
 *
 */
public class Bank extends PadBank<Patch,CControl> {
	
	public Bank()
	{
		patch = new ArrayList<Patch>();
	}
	
	public Bank(int nPadPerPatch)
	{
		patch = new ArrayList<Patch>();
		patch.add(new Patch("default", /*nPadPerPatch,*/ true));
	}
	
	/**
	 * @return
	 */
	public Patch newPatch()
	{
		int n = patch.size();
		Patch p = new Patch("default"+Integer.toString(n)/*, nPad*/, true);
		for (int i=0; i<MainActivity.Global.N_PAD; i++) {
			p.add(new PadSampleState());
		}
		return p;
	}

	/****************************************
	 * parser stuff
	 ****************************************/
	
	/**
	 * @param cca
	 * @param v
	 * @return
	 */
	private static CControl findInCCA(ControlsAdapter cca, String v)
	{
		if (cca == null || v == null) {
			return null;
		}
		for (int i=0; i<cca.getCount(); i++) {
			Control cc = cca.getItem(i);
			if (cc != null && v.equals(cc.ccontrolName())) {
				return (CControl) cc;
			}
		}
		return null;
	}


	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private Patch doPatch(XmlPullParser xpp) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("patch")) return null;
		
		Patch p = new Patch();
		PadSampleState pss = null;
		BusState bss = null;
		XYControlAssign xss = null;
		SXControlAssign sss = null;
		RXControlAssign rrr = null;
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("name")){
				p.name = v;
				/*
			} else if (n.equals("tempo")){
				p.tempo = Float.parseFloat(v);
				*/
			} else {
//				Log.d("bank error", "Unknown attribute in 'patch' element, "+n);
//				throw new XmlPullParserException("Unknown attribute in patch element, "+n);
			}
		}
		
//		Log.d("do patch", String.format("got patch name %s", p.name));
		
		if (xpp.isEmptyElementTag()) {
			return p;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if ((pss=doPad(xpp, null)) != null) {
					p.add(pss);
					/*
				} else if ((xss=doController(xpp, SpaceGun.sg)) != null) {
					p.add(xss);
				} else if ((bss=doBus(xpp)) != null) {
					p.add(bss);
				} else if ((sss=doSensor(xpp, SpaceGun.sg, SpaceGun.sg)) != null) {
					p.add(sss);
				} else if ((rrr=doRibbon(xpp, SpaceGun.sg)) != null) {
					p.add(rrr);
					*/
				} else {
					Log.d("bank", String.format("unexpected start tag %s", xpp.getName()));
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("patch")) { // tag consumed in loop of doBank()
					return p;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'patch'");
	}
	
	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected boolean doBank(XmlPullParser xpp) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("bank")) return false;
		
		if (xpp.isEmptyElementTag()) {
			return true;
		}
		
		int na=xpp.getAttributeCount();
		String source = null;
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("source")){
				source = v;
			}
		}
		if (source == null || !source.equals("spacegun")) {
			return false;
		}
		Patch p = null;
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if ((p=doPatch(xpp)) != null) {
					patch.add(p);
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("bank")) { // this will be consumed by the 'next()' call in the current iteration of the loop of load() 
					return true;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'bank'");
	}

	/**
	 * @param fp
	 * @return
	 */
	public boolean save(FileOutputStream fp)
	{
		XmlSerializer xml = Xml.newSerializer();
		try {
			FileWriter fw = new FileWriter(fp.getFD());
			xml.setOutput(fw);
			xml.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			xml.startDocument("UTF-8", true);
			xs(xml, "bank");
			sa(xml, "source", "spacegun");
			la(xml, "version", 1);
			for (Patch p: patch) {
				xs(xml, "patch");
				sa(xml, "name", p.name);
				/*
				fa(xml, "tempo", p.tempo);
				*/
				for (PadSampleState pss: p.padState) {
					putPad(xml, pss);
				}			
/*
 *  bus and effects
 *
 */
				/*
				Log.d("save", String.format("bus %d", p.busState.size()));
				for (BusState b: p.busState) {
					putBus(xml, b);
				}*/
/*
 *  controllers
 */
				/*
				int i=1;
				for (XYControlAssign pss: p.touchAssign) {
					if (pss != null && pss.isValid()) {
						xs(xml, "controller");
						
						sa(xml, "ctlType", "touch");
						la(xml, "ctlId", 1);
						
						sa(xml, "dstType", pss.controllable.controllableType());
						la(xml, "dstId", pss.controllable.controllableId());
	
						String t = pss.xp.ccontrolName();
						int cid = pss.xp.instanceId();
						if (t != null) sa(xml, "xpType", t);
						if (cid > 0) la(xml, "xpId", cid);
						
						t = pss.yp.ccontrolName();
						cid = pss.yp.instanceId();
						if (t != null) sa(xml, "ypType", t);
						if (cid > 0) la(xml, "ypId", cid);
						
						xe(xml, "controller");
					}
					i++;
				}
				for (SXControlAssign pss: p.sensorAssign) {
					if (pss != null && pss.isValid()) {
//						Log.d("save", String.format("sensors %s", pss.controllable.controllableType()));
						xs(xml, "sensor");
						
						sa(xml, "ctlType", pss.getSensorTag());
						la(xml, "ctlId", 1);
						
						sa(xml, "dstType", pss.controllable.controllableType());
						la(xml, "dstId", pss.controllable.controllableId());
	
						String t = pss.xp.ccontrolName();
						int cid = pss.xp.instanceId();
						if (t != null) sa(xml, "xpType", t);
						if (cid > 0) la(xml, "xpId", cid);
						
						fa(xml, "mapMin", pss.mapMin);
						fa(xml, "mapCenter", pss.mapCenter);
						fa(xml, "mapMax", pss.mapMax);
						fa(xml, "rangeMin", pss.rangeMin);
						fa(xml, "rangeCenter", pss.rangeCenter);
						fa(xml, "rangeMax", pss.rangeMax);
						
						xe(xml, "sensor");
					}
				}			
				for (RXControlAssign pss: p.ribbonAssign) {
					if (pss != null && pss.isValid()) {
//						Log.d("save", String.format("ribbons %s", pss.controllable.controllableType()));
						xs(xml, "ribbon");
						
						sa(xml, "ctlType", "ribbon");
						la(xml, "ctlId", 1);
						
						sa(xml, "dstType", pss.controllable.controllableType());
						la(xml, "dstId", pss.controllable.controllableId());
	
						String t = pss.xp.ccontrolName();
						int cid = pss.xp.instanceId();
						if (t != null) sa(xml, "xpType", t);
						if (cid > 0) la(xml, "xpId", cid);
						
						fa(xml, "mapMin", pss.mapMin);
						fa(xml, "mapCenter", pss.mapCenter);
						fa(xml, "mapMax", pss.mapMax);
						fa(xml, "rangeMin", pss.rangeMin);
						fa(xml, "rangeCenter", pss.rangeCenter);
						fa(xml, "rangeMax", pss.rangeMax);
						
						xe(xml, "ribbon");
					}
				}
				*/
				xe(xml, "patch");
			}
			xe(xml, "bank");
			xml.endDocument();
			fw.close();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return false;
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public CControl createControl(String s) {
		return new CControl(s);
	}
	
	@Override
	protected ArrayList<FX> fxList()
	{
		return MWUtils.defaultFX();
	}

	@Override
	public Patch clonePatch(int i) {
		Patch p = get(i);
		return (p!=null)? p.clone(): null;
	}

	@Override
	public Patch saveCopy(int i, Patch p) {
		return set(i, p.clone());
	}



}
