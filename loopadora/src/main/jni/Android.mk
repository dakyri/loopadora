LOCAL_PATH:= $(call my-dir)

CORE_AUDIO_BASE:=../../MayaswellCore/jni/audio
CORE_AUDIO_OBJ:=../../MayaswellCore/obj/local/armeabi
CPAD_BASE:=../../SpaceGunLib/jni
CPAD_OBJ:=../../SpaceGunLib/obj/local/armeabi

include $(CLEAR_VARS)
LOCAL_MODULE := cpad
LOCAL_SRC_FILES := $(CPAD_OBJ)/libcpad.so
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/$(CPAD_BASE)/include
include $(PREBUILT_SHARED_LIBRARY)
